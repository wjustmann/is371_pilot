﻿Public Class Service
    Dim index As Integer
    Private Sub Service_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Me.MdiParent = Form1
        Me.Dock = DockStyle.Fill
        Me.FormBorderStyle = Windows.Forms.FormBorderStyle.None
        Me.PartsTableAdapter.Fill(Me.AvailableSetDataSet.Parts)

        ' fill the list view column headers
        For i As Integer = 0 To AvailableSetDataSet.Tables(0).Columns.Count - 1
            ListView1.Columns.Add(AvailableSetDataSet.Tables(0).Columns(i).ToString())
        Next
        ' fill the list view with data
        For i As Integer = 0 To AvailableSetDataSet.Tables(0).Rows.Count - 1
            Dim listRow As New ListViewItem
            listRow.Text = AvailableSetDataSet.Tables(0).Rows(i)(0).ToString()
            For j As Integer = 1 To AvailableSetDataSet.Tables(0).Columns.Count - 1
                listRow.SubItems.Add(AvailableSetDataSet.Tables(0).Rows(i)(j).ToString())
            Next
            ListView1.Items.Add(listRow)
        Next
        ListView1.View = View.Tile
        ListView1.Items(0).Selected = True
        ListView1.Select()

        ' fill the tree view
        BuildTree(AvailableSetDataSet.Tables(0), TreeView1, True)
        TreeView1.SelectedNode = TreeView1.TopNode

    End Sub


    ''' <summary>
    ''' This method sets the image for the selected item in the datagridview
    ''' </summary>
    Private Sub DataGridView1_CellClick(ByVal sender As System.Object, ByVal e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellClick
        ' display the image and part information
        index = DataGridView1.CurrentCell.RowIndex
        Select Case index
            Case 0
                DataGridPartsImage.Image = My.Resources._1
            Case 1
                DataGridPartsImage.Image = My.Resources._2
            Case 2
                DataGridPartsImage.Image = My.Resources._3
            Case 3
                DataGridPartsImage.Image = My.Resources._4
            Case 4
                DataGridPartsImage.Image = My.Resources._5
            Case 5
                DataGridPartsImage.Image = My.Resources._6
            Case 6
                DataGridPartsImage.Image = My.Resources._7
            Case 7
                DataGridPartsImage.Image = My.Resources._8
            Case 8
                DataGridPartsImage.Image = My.Resources._9
            Case 9
                DataGridPartsImage.Image = My.Resources._10
            Case 10
                DataGridPartsImage.Image = My.Resources._11
            Case 11
                DataGridPartsImage.Image = My.Resources._12
            Case 12
                DataGridPartsImage.Image = My.Resources._13
            Case 13
                DataGridPartsImage.Image = My.Resources._14
            Case 14
                DataGridPartsImage.Image = My.Resources._15
            Case 15
                DataGridPartsImage.Image = My.Resources._16
            Case 16
                DataGridPartsImage.Image = My.Resources._17
        End Select
        DataGridPartsImage.SizeMode = PictureBoxSizeMode.StretchImage

    End Sub


    ''' <summary>
    ''' This method sets the image for the selected item in the listview
    ''' </summary>
    Private Sub ListView1_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListView1.SelectedIndexChanged
        ' display image in box
        Dim index As Integer
        Try
            index = ListView1.FocusedItem.Index
        Catch ex As Exception
            Return
        End Try
        Select Case index
            Case 0
                ListViewPartsImage.Image = My.Resources._1
            Case 1
                ListViewPartsImage.Image = My.Resources._2
            Case 2
                ListViewPartsImage.Image = My.Resources._3
            Case 3
                ListViewPartsImage.Image = My.Resources._4
            Case 4
                ListViewPartsImage.Image = My.Resources._5
            Case 5
                ListViewPartsImage.Image = My.Resources._6
            Case 6
                ListViewPartsImage.Image = My.Resources._7
            Case 7
                ListViewPartsImage.Image = My.Resources._8
            Case 8
                ListViewPartsImage.Image = My.Resources._9
            Case 9
                ListViewPartsImage.Image = My.Resources._10
            Case 10
                ListViewPartsImage.Image = My.Resources._11
            Case 11
                ListViewPartsImage.Image = My.Resources._12
            Case 12
                ListViewPartsImage.Image = My.Resources._13
            Case 13
                ListViewPartsImage.Image = My.Resources._14
            Case 14
                ListViewPartsImage.Image = My.Resources._15
            Case 15
                ListViewPartsImage.Image = My.Resources._16
            Case 16
                ListViewPartsImage.Image = My.Resources._17
        End Select
        ListViewPartsImage.SizeMode = PictureBoxSizeMode.StretchImage
    End Sub

    ''' <summary>
    ''' This method sets the image for the selected item in the treeview
    ''' </summary>
    Private Sub TreeView1_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) Handles TreeView1.AfterSelect
        ' display image in box
        Dim index As Integer
        index = e.Node.Index
        Try
            If (index = 0) Then
                index = e.Node.Parent.Index
            End If
        Catch ex As Exception
            ' leave index at 0, this is the top node
        End Try
       
        Select Case index
            Case 0
                TreeViewPartsImage.Image = My.Resources._1
            Case 1
                TreeViewPartsImage.Image = My.Resources._2
            Case 2
                TreeViewPartsImage.Image = My.Resources._3
            Case 3
                TreeViewPartsImage.Image = My.Resources._4
            Case 4
                TreeViewPartsImage.Image = My.Resources._5
            Case 5
                TreeViewPartsImage.Image = My.Resources._6
            Case 6
                TreeViewPartsImage.Image = My.Resources._7
            Case 7
                TreeViewPartsImage.Image = My.Resources._8
            Case 8
                TreeViewPartsImage.Image = My.Resources._9
            Case 9
                TreeViewPartsImage.Image = My.Resources._10
            Case 10
                TreeViewPartsImage.Image = My.Resources._11
            Case 11
                TreeViewPartsImage.Image = My.Resources._12
            Case 12
                TreeViewPartsImage.Image = My.Resources._13
            Case 13
                TreeViewPartsImage.Image = My.Resources._14
            Case 14
                TreeViewPartsImage.Image = My.Resources._15
            Case 15
                TreeViewPartsImage.Image = My.Resources._16
            Case 16
                TreeViewPartsImage.Image = My.Resources._17
        End Select
        TreeViewPartsImage.SizeMode = PictureBoxSizeMode.StretchImage
    End Sub

    ' http://www.authorcode.com/create-treeview-from-datatable-using-vb-net/
    Public Sub BuildTree(ByVal dt As DataTable, ByVal trv As TreeView, ByVal expandAll As [Boolean])
        ' Clear the TreeView if there are another datas in this TreeView
        trv.Nodes.Clear()
        Dim node As TreeNode
        Dim subNode As TreeNode
        For Each row As DataRow In dt.Rows
            node = Searchnode(row.Item(0).ToString(), trv)
            If node IsNot Nothing Then
                subNode = New TreeNode(row.Item(1).ToString())
                node.Nodes.Add(subNode)
            Else
                node = New TreeNode(row.Item(0).ToString())
                subNode = New TreeNode(row.Item(1).ToString())
                node.Nodes.Add(subNode)
                trv.Nodes.Add(node)
            End If
        Next
        If expandAll Then
            ' Expand the TreeView
            trv.ExpandAll()
        End If
    End Sub
    ' http://www.authorcode.com/create-treeview-from-datatable-using-vb-net/
    Private Function Searchnode(ByVal nodetext As String, ByVal trv As TreeView) As TreeNode
        For Each node As TreeNode In trv.Nodes
            If node.Text = nodetext Then
                Return node
            End If
        Next
        Return Nothing
    End Function

    ''' <summary>
    ''' This method advances the currently selected cell of the DataViewGrid
    ''' </summary>
    Private Sub DataGridForward_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridForward.Click
        If (DataGridView1.CurrentRow.Index = DataGridView1.Rows.Count - 1) Then
            Return
        End If
        DataGridView1.CurrentCell = DataGridView1.Rows(DataGridView1.CurrentRow.Index + 1).Cells(0)
        DataGridView1_CellClick(sender, Nothing)
    End Sub

    ''' <summary>
    ''' This method decrements the currently selected cell of the DataViewGrid
    ''' </summary>
    Private Sub DataGridBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles DataGridBack.Click
        If (DataGridView1.CurrentRow.Index = 0) Then
            Return
        End If
        DataGridView1.CurrentCell = DataGridView1.Rows(DataGridView1.CurrentRow.Index - 1).Cells(0)
        DataGridView1_CellClick(sender, Nothing)
    End Sub

    ''' <summary>
    ''' This method advances the currently selected cell of the TreeView
    ''' </summary>
    Private Sub TreeViewForward_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TreeViewForward.Click
        Try
            TreeView1.SelectedNode = TreeView1.SelectedNode.NextNode
        Catch ex As Exception

        End Try

    End Sub

    ''' <summary>
    ''' this method decrements the currently selectedc cell of the TreeView
    ''' </summary>
    Private Sub TreeViewBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles TreeViewBack.Click
        Try
            TreeView1.SelectedNode = TreeView1.SelectedNode.PrevNode
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' This method advances the currently selected item in the listview
    ''' </summary>
    Private Sub ListViewForward_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListViewForward.Click
        Try
            ListView1.Items(ListView1.FocusedItem.Index + 1).Selected = True
            ListView1.Select()
            ListView1.FocusedItem = ListView1.SelectedItems(0)
        Catch ex As Exception

        End Try
    End Sub

    ''' <summary>
    ''' This method decrements the currently selected item in the listview
    ''' </summary>
    Private Sub ListViewBack_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ListViewBack.Click
        Try
            ListView1.Items(ListView1.FocusedItem.Index - 1).Selected = True
            ListView1.Select()
            ListView1.FocusedItem = ListView1.SelectedItems(0)
        Catch ex As Exception

        End Try
    End Sub

End Class
