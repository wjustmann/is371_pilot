﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Orders
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.AvailableSetDataSet = New AvailableSetDataSet()
        Me.OrderCostBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.OrderCostTableAdapter = New AvailableSetDataSetTableAdapters.OrderCostTableAdapter()
        Me.WORKORDERIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PARTNAMEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRICEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.QTYDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AvailableSetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.OrderCostBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.WORKORDERIDDataGridViewTextBoxColumn, Me.PARTNAMEDataGridViewTextBoxColumn, Me.PRICEDataGridViewTextBoxColumn, Me.QTYDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.OrderCostBindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(9, 5)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        Me.DataGridView1.Size = New System.Drawing.Size(545, 290)
        Me.DataGridView1.TabIndex = 0
        '
        'AvailableSetDataSet
        '
        Me.AvailableSetDataSet.DataSetName = "AvailableSetDataSet"
        Me.AvailableSetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'OrderCostBindingSource
        '
        Me.OrderCostBindingSource.DataMember = "OrderCost"
        Me.OrderCostBindingSource.DataSource = Me.AvailableSetDataSet
        '
        'OrderCostTableAdapter
        '
        Me.OrderCostTableAdapter.ClearBeforeFill = True
        '
        'WORKORDERIDDataGridViewTextBoxColumn
        '
        Me.WORKORDERIDDataGridViewTextBoxColumn.DataPropertyName = "WORK_ORDER_ID"
        Me.WORKORDERIDDataGridViewTextBoxColumn.HeaderText = "WORK_ORDER_ID"
        Me.WORKORDERIDDataGridViewTextBoxColumn.Name = "WORKORDERIDDataGridViewTextBoxColumn"
        Me.WORKORDERIDDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PARTNAMEDataGridViewTextBoxColumn
        '
        Me.PARTNAMEDataGridViewTextBoxColumn.DataPropertyName = "PART_NAME"
        Me.PARTNAMEDataGridViewTextBoxColumn.HeaderText = "PART_NAME"
        Me.PARTNAMEDataGridViewTextBoxColumn.Name = "PARTNAMEDataGridViewTextBoxColumn"
        Me.PARTNAMEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PRICEDataGridViewTextBoxColumn
        '
        Me.PRICEDataGridViewTextBoxColumn.DataPropertyName = "PRICE"
        Me.PRICEDataGridViewTextBoxColumn.HeaderText = "PRICE"
        Me.PRICEDataGridViewTextBoxColumn.Name = "PRICEDataGridViewTextBoxColumn"
        Me.PRICEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'QTYDataGridViewTextBoxColumn
        '
        Me.QTYDataGridViewTextBoxColumn.DataPropertyName = "QTY"
        Me.QTYDataGridViewTextBoxColumn.HeaderText = "QTY"
        Me.QTYDataGridViewTextBoxColumn.Name = "QTYDataGridViewTextBoxColumn"
        Me.QTYDataGridViewTextBoxColumn.ReadOnly = True
        '
        'Orders
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(570, 312)
        Me.Controls.Add(Me.DataGridView1)
        Me.Name = "Orders"
        Me.Text = "Orders"
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AvailableSetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.OrderCostBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents AvailableSetDataSet As AvailableSetDataSet
    Friend WithEvents OrderCostBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents OrderCostTableAdapter As AvailableSetDataSetTableAdapters.OrderCostTableAdapter
    Friend WithEvents WORKORDERIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PARTNAMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRICEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents QTYDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
End Class
