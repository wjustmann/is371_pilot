﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Service
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Dim DataGridViewCellStyle4 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle5 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Dim DataGridViewCellStyle6 As System.Windows.Forms.DataGridViewCellStyle = New System.Windows.Forms.DataGridViewCellStyle()
        Me.TabControl1 = New System.Windows.Forms.TabControl()
        Me.TabPage1 = New System.Windows.Forms.TabPage()
        Me.ListViewForward = New System.Windows.Forms.Button()
        Me.ListViewBack = New System.Windows.Forms.Button()
        Me.ListViewPartsImage = New System.Windows.Forms.PictureBox()
        Me.ListView1 = New System.Windows.Forms.ListView()
        Me.TabPage2 = New System.Windows.Forms.TabPage()
        Me.TreeViewForward = New System.Windows.Forms.Button()
        Me.TreeViewBack = New System.Windows.Forms.Button()
        Me.TreeViewPartsImage = New System.Windows.Forms.PictureBox()
        Me.TreeView1 = New System.Windows.Forms.TreeView()
        Me.TabPage3 = New System.Windows.Forms.TabPage()
        Me.DataGridForward = New System.Windows.Forms.Button()
        Me.DataGridBack = New System.Windows.Forms.Button()
        Me.DataGridPartsImage = New System.Windows.Forms.PictureBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.PARTIDDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PARTNAMEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PARTCATNAMEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.LABORDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PRICEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.DESCDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PartsBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AvailableSetDataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.AvailableSetDataSet = New AvailableSetDataSet()
        Me.PartsTableAdapter = New AvailableSetDataSetTableAdapters.PartsTableAdapter()
        Me.TabControl1.SuspendLayout()
        Me.TabPage1.SuspendLayout()
        CType(Me.ListViewPartsImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage2.SuspendLayout()
        CType(Me.TreeViewPartsImage, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TabPage3.SuspendLayout()
        CType(Me.DataGridPartsImage, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.PartsBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AvailableSetDataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.AvailableSetDataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'TabControl1
        '
        Me.TabControl1.Controls.Add(Me.TabPage1)
        Me.TabControl1.Controls.Add(Me.TabPage2)
        Me.TabControl1.Controls.Add(Me.TabPage3)
        Me.TabControl1.Location = New System.Drawing.Point(12, 19)
        Me.TabControl1.Name = "TabControl1"
        Me.TabControl1.SelectedIndex = 0
        Me.TabControl1.Size = New System.Drawing.Size(680, 295)
        Me.TabControl1.TabIndex = 0
        '
        'TabPage1
        '
        Me.TabPage1.Controls.Add(Me.ListViewForward)
        Me.TabPage1.Controls.Add(Me.ListViewBack)
        Me.TabPage1.Controls.Add(Me.ListViewPartsImage)
        Me.TabPage1.Controls.Add(Me.ListView1)
        Me.TabPage1.Location = New System.Drawing.Point(4, 22)
        Me.TabPage1.Name = "TabPage1"
        Me.TabPage1.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage1.Size = New System.Drawing.Size(672, 269)
        Me.TabPage1.TabIndex = 0
        Me.TabPage1.Text = "List View Parts"
        Me.TabPage1.UseVisualStyleBackColor = True
        '
        'ListViewForward
        '
        Me.ListViewForward.Location = New System.Drawing.Point(567, 233)
        Me.ListViewForward.Name = "ListViewForward"
        Me.ListViewForward.Size = New System.Drawing.Size(81, 22)
        Me.ListViewForward.TabIndex = 3
        Me.ListViewForward.Text = "Forward"
        Me.ListViewForward.UseVisualStyleBackColor = True
        '
        'ListViewBack
        '
        Me.ListViewBack.Location = New System.Drawing.Point(285, 233)
        Me.ListViewBack.Name = "ListViewBack"
        Me.ListViewBack.Size = New System.Drawing.Size(81, 22)
        Me.ListViewBack.TabIndex = 2
        Me.ListViewBack.Text = "Back"
        Me.ListViewBack.UseVisualStyleBackColor = True
        '
        'ListViewPartsImage
        '
        Me.ListViewPartsImage.Location = New System.Drawing.Point(285, 15)
        Me.ListViewPartsImage.Name = "ListViewPartsImage"
        Me.ListViewPartsImage.Size = New System.Drawing.Size(363, 210)
        Me.ListViewPartsImage.TabIndex = 1
        Me.ListViewPartsImage.TabStop = False
        '
        'ListView1
        '
        Me.ListView1.Location = New System.Drawing.Point(10, 12)
        Me.ListView1.MultiSelect = False
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(253, 256)
        Me.ListView1.TabIndex = 0
        Me.ListView1.UseCompatibleStateImageBehavior = False
        '
        'TabPage2
        '
        Me.TabPage2.Controls.Add(Me.TreeViewForward)
        Me.TabPage2.Controls.Add(Me.TreeViewBack)
        Me.TabPage2.Controls.Add(Me.TreeViewPartsImage)
        Me.TabPage2.Controls.Add(Me.TreeView1)
        Me.TabPage2.Location = New System.Drawing.Point(4, 22)
        Me.TabPage2.Name = "TabPage2"
        Me.TabPage2.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage2.Size = New System.Drawing.Size(672, 269)
        Me.TabPage2.TabIndex = 1
        Me.TabPage2.Text = "Tree View Parts"
        Me.TabPage2.UseVisualStyleBackColor = True
        '
        'TreeViewForward
        '
        Me.TreeViewForward.Location = New System.Drawing.Point(585, 235)
        Me.TreeViewForward.Name = "TreeViewForward"
        Me.TreeViewForward.Size = New System.Drawing.Size(81, 22)
        Me.TreeViewForward.TabIndex = 5
        Me.TreeViewForward.Text = "Forward"
        Me.TreeViewForward.UseVisualStyleBackColor = True
        '
        'TreeViewBack
        '
        Me.TreeViewBack.Location = New System.Drawing.Point(303, 235)
        Me.TreeViewBack.Name = "TreeViewBack"
        Me.TreeViewBack.Size = New System.Drawing.Size(81, 22)
        Me.TreeViewBack.TabIndex = 4
        Me.TreeViewBack.Text = "Back"
        Me.TreeViewBack.UseVisualStyleBackColor = True
        '
        'TreeViewPartsImage
        '
        Me.TreeViewPartsImage.Location = New System.Drawing.Point(303, 13)
        Me.TreeViewPartsImage.Name = "TreeViewPartsImage"
        Me.TreeViewPartsImage.Size = New System.Drawing.Size(363, 216)
        Me.TreeViewPartsImage.TabIndex = 1
        Me.TreeViewPartsImage.TabStop = False
        '
        'TreeView1
        '
        Me.TreeView1.Location = New System.Drawing.Point(12, 6)
        Me.TreeView1.Name = "TreeView1"
        Me.TreeView1.Size = New System.Drawing.Size(273, 251)
        Me.TreeView1.TabIndex = 0
        '
        'TabPage3
        '
        Me.TabPage3.Controls.Add(Me.DataGridForward)
        Me.TabPage3.Controls.Add(Me.DataGridBack)
        Me.TabPage3.Controls.Add(Me.DataGridPartsImage)
        Me.TabPage3.Controls.Add(Me.DataGridView1)
        Me.TabPage3.Location = New System.Drawing.Point(4, 22)
        Me.TabPage3.Name = "TabPage3"
        Me.TabPage3.Padding = New System.Windows.Forms.Padding(3)
        Me.TabPage3.Size = New System.Drawing.Size(672, 269)
        Me.TabPage3.TabIndex = 2
        Me.TabPage3.Text = "DataGrid Parts"
        Me.TabPage3.UseVisualStyleBackColor = True
        '
        'DataGridForward
        '
        Me.DataGridForward.Location = New System.Drawing.Point(585, 236)
        Me.DataGridForward.Name = "DataGridForward"
        Me.DataGridForward.Size = New System.Drawing.Size(81, 22)
        Me.DataGridForward.TabIndex = 7
        Me.DataGridForward.Text = "Forward"
        Me.DataGridForward.UseVisualStyleBackColor = True
        '
        'DataGridBack
        '
        Me.DataGridBack.Location = New System.Drawing.Point(351, 236)
        Me.DataGridBack.Name = "DataGridBack"
        Me.DataGridBack.Size = New System.Drawing.Size(81, 22)
        Me.DataGridBack.TabIndex = 6
        Me.DataGridBack.Text = "Back"
        Me.DataGridBack.UseVisualStyleBackColor = True
        '
        'DataGridPartsImage
        '
        Me.DataGridPartsImage.Location = New System.Drawing.Point(351, 16)
        Me.DataGridPartsImage.Name = "DataGridPartsImage"
        Me.DataGridPartsImage.Size = New System.Drawing.Size(315, 214)
        Me.DataGridPartsImage.TabIndex = 1
        Me.DataGridPartsImage.TabStop = False
        '
        'DataGridView1
        '
        Me.DataGridView1.AllowUserToAddRows = False
        Me.DataGridView1.AllowUserToDeleteRows = False
        Me.DataGridView1.AutoGenerateColumns = False
        DataGridViewCellStyle4.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle4.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle4.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle4.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle4.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle4.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle4.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.ColumnHeadersDefaultCellStyle = DataGridViewCellStyle4
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.PARTIDDataGridViewTextBoxColumn, Me.PARTNAMEDataGridViewTextBoxColumn, Me.PARTCATNAMEDataGridViewTextBoxColumn, Me.LABORDataGridViewTextBoxColumn, Me.PRICEDataGridViewTextBoxColumn, Me.DESCDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.PartsBindingSource
        DataGridViewCellStyle5.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle5.BackColor = System.Drawing.SystemColors.Window
        DataGridViewCellStyle5.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle5.ForeColor = System.Drawing.SystemColors.ControlText
        DataGridViewCellStyle5.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle5.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle5.WrapMode = System.Windows.Forms.DataGridViewTriState.[False]
        Me.DataGridView1.DefaultCellStyle = DataGridViewCellStyle5
        Me.DataGridView1.Location = New System.Drawing.Point(15, 16)
        Me.DataGridView1.MultiSelect = False
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.ReadOnly = True
        DataGridViewCellStyle6.Alignment = System.Windows.Forms.DataGridViewContentAlignment.MiddleLeft
        DataGridViewCellStyle6.BackColor = System.Drawing.SystemColors.Control
        DataGridViewCellStyle6.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        DataGridViewCellStyle6.ForeColor = System.Drawing.SystemColors.WindowText
        DataGridViewCellStyle6.SelectionBackColor = System.Drawing.SystemColors.Highlight
        DataGridViewCellStyle6.SelectionForeColor = System.Drawing.SystemColors.HighlightText
        DataGridViewCellStyle6.WrapMode = System.Windows.Forms.DataGridViewTriState.[True]
        Me.DataGridView1.RowHeadersDefaultCellStyle = DataGridViewCellStyle6
        Me.DataGridView1.Size = New System.Drawing.Size(320, 242)
        Me.DataGridView1.TabIndex = 0
        '
        'PARTIDDataGridViewTextBoxColumn
        '
        Me.PARTIDDataGridViewTextBoxColumn.DataPropertyName = "PART_ID"
        Me.PARTIDDataGridViewTextBoxColumn.HeaderText = "PART_ID"
        Me.PARTIDDataGridViewTextBoxColumn.Name = "PARTIDDataGridViewTextBoxColumn"
        Me.PARTIDDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PARTNAMEDataGridViewTextBoxColumn
        '
        Me.PARTNAMEDataGridViewTextBoxColumn.DataPropertyName = "PART_NAME"
        Me.PARTNAMEDataGridViewTextBoxColumn.HeaderText = "PART_NAME"
        Me.PARTNAMEDataGridViewTextBoxColumn.Name = "PARTNAMEDataGridViewTextBoxColumn"
        Me.PARTNAMEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PARTCATNAMEDataGridViewTextBoxColumn
        '
        Me.PARTCATNAMEDataGridViewTextBoxColumn.DataPropertyName = "PART_CAT_NAME"
        Me.PARTCATNAMEDataGridViewTextBoxColumn.HeaderText = "PART_CAT_NAME"
        Me.PARTCATNAMEDataGridViewTextBoxColumn.Name = "PARTCATNAMEDataGridViewTextBoxColumn"
        Me.PARTCATNAMEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'LABORDataGridViewTextBoxColumn
        '
        Me.LABORDataGridViewTextBoxColumn.DataPropertyName = "LABOR"
        Me.LABORDataGridViewTextBoxColumn.HeaderText = "LABOR"
        Me.LABORDataGridViewTextBoxColumn.Name = "LABORDataGridViewTextBoxColumn"
        Me.LABORDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PRICEDataGridViewTextBoxColumn
        '
        Me.PRICEDataGridViewTextBoxColumn.DataPropertyName = "PRICE"
        Me.PRICEDataGridViewTextBoxColumn.HeaderText = "PRICE"
        Me.PRICEDataGridViewTextBoxColumn.Name = "PRICEDataGridViewTextBoxColumn"
        Me.PRICEDataGridViewTextBoxColumn.ReadOnly = True
        '
        'DESCDataGridViewTextBoxColumn
        '
        Me.DESCDataGridViewTextBoxColumn.DataPropertyName = "DESC"
        Me.DESCDataGridViewTextBoxColumn.HeaderText = "DESC"
        Me.DESCDataGridViewTextBoxColumn.Name = "DESCDataGridViewTextBoxColumn"
        Me.DESCDataGridViewTextBoxColumn.ReadOnly = True
        '
        'PartsBindingSource
        '
        Me.PartsBindingSource.DataMember = "Parts"
        Me.PartsBindingSource.DataSource = Me.AvailableSetDataSetBindingSource
        '
        'AvailableSetDataSetBindingSource
        '
        Me.AvailableSetDataSetBindingSource.DataSource = Me.AvailableSetDataSet
        Me.AvailableSetDataSetBindingSource.Position = 0
        '
        'AvailableSetDataSet
        '
        Me.AvailableSetDataSet.DataSetName = "AvailableSetDataSet"
        Me.AvailableSetDataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'PartsTableAdapter
        '
        Me.PartsTableAdapter.ClearBeforeFill = True
        '
        'Service
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(704, 326)
        Me.Controls.Add(Me.TabControl1)
        Me.Name = "Service"
        Me.Text = "Service"
        Me.TabControl1.ResumeLayout(False)
        Me.TabPage1.ResumeLayout(False)
        CType(Me.ListViewPartsImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage2.ResumeLayout(False)
        CType(Me.TreeViewPartsImage, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TabPage3.ResumeLayout(False)
        CType(Me.DataGridPartsImage, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.PartsBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AvailableSetDataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.AvailableSetDataSet, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents TabControl1 As System.Windows.Forms.TabControl
    Friend WithEvents TabPage1 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage2 As System.Windows.Forms.TabPage
    Friend WithEvents TabPage3 As System.Windows.Forms.TabPage
    Friend WithEvents DataGridView1 As System.Windows.Forms.DataGridView
    Friend WithEvents AvailableSetDataSetBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents AvailableSetDataSet As AvailableSetDataSet
    Friend WithEvents PartsBindingSource As System.Windows.Forms.BindingSource
    Friend WithEvents PartsTableAdapter As AvailableSetDataSetTableAdapters.PartsTableAdapter
    Friend WithEvents PARTIDDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PARTNAMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PARTCATNAMEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents LABORDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents PRICEDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents DESCDataGridViewTextBoxColumn As System.Windows.Forms.DataGridViewTextBoxColumn
    Friend WithEvents ListView1 As System.Windows.Forms.ListView
    Friend WithEvents TreeView1 As System.Windows.Forms.TreeView
    Friend WithEvents DataGridPartsImage As System.Windows.Forms.PictureBox
    Friend WithEvents ListViewPartsImage As System.Windows.Forms.PictureBox
    Friend WithEvents TreeViewPartsImage As System.Windows.Forms.PictureBox
    Friend WithEvents ListViewForward As System.Windows.Forms.Button
    Friend WithEvents ListViewBack As System.Windows.Forms.Button
    Friend WithEvents TreeViewForward As System.Windows.Forms.Button
    Friend WithEvents TreeViewBack As System.Windows.Forms.Button
    Friend WithEvents DataGridForward As System.Windows.Forms.Button
    Friend WithEvents DataGridBack As System.Windows.Forms.Button

End Class
